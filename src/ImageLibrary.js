import React from "react";
import './App.css';

function getMemeImageElements(allImages, handleImgClick) {
  //As a functional component, props are being passed in as parameters of this funtion.
  let imgStyles = {
    cursor: "pointer",
    width: "50px",
    height: "50px"
  };

  let images;
  
  if (allImages) {
    images = allImages.map(image => (
      <a key={image.id} href="#top-container">
        <img //assigning propperties of the images from the api to variables we can call/use .
          id={image.id}
          src={image.url}
          alt={image.name}
          style={imgStyles}
          onClick={handleImgClick}
        />
      </a>
    ));

    return images;
  }
  return null;
}

function ImageLibrary(props) {
  const { allImages, handleImgClick } = props;
   //defining allimages and handleimgclick as props.

  const memeImages = getMemeImageElements(allImages, handleImgClick); 
  //assigning fucntion above with props to memeImages.

  return (
    <div className="meme-library-container" id="meme-library-container">
      <h2>Meme Library</h2>
      <div className="meme-library">{memeImages}</div>
    </div>
  );
}

export default ImageLibrary;

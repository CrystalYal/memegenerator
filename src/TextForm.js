import React from "react";
import './App.css';

function TextForm(props) {
  const { userText, handleTextChange, } = props;
  return (
    <div className="text-form" id="drag">
      <input
        name="userText"
        value={userText}
        onChange={handleTextChange}
        placeholder="Enter text "
      />
</div>
  );
}

export default TextForm;
import React, { Component } from "react";
import "./App.css";

class CurrentImage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    
  }

  componentDidMount(){
    this.dragElement(document.getElementById("toptext"));
    this.dragElement(document.getElementById("bottomtext"));
    this.dragElement(document.getElementById("toptexttwo"));
    this.dragElement(document.getElementById("bottomtexttwo"));
  }


   // collected from https://www.w3schools.com/howto/howto_js_draggable.asp
  dragElement(elmnt) {
    var pos1 = 0,
      pos2 = 0,
      pos3 = 0,
      pos4 = 0;
    
      elmnt.onmousedown = dragMouseDown;
   

    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      // get the mouse cursor position at startup:
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      // set the element's new position:
      elmnt.style.top = elmnt.offsetTop - pos2 + "px";
      elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
    }

    function closeDragElement() {
      // stop moving when mouse button is released:
      document.onmouseup = null;
      document.onmousemove = null;
    }
  }

  render() {
    // console.log(this.props.currentImage);
    const { src, alt } = this.props.currentImage;
    //The alt property sets or returns the value of the alt attribute of an image.
    //The required alt attribute specifies an alternate text for an image.

    return (
      <div className="current-image" id="current-image">
        {!src ? ( //if src is empty
          <h2>
            <span style={{ fontSize: "10px" }}>
              "Click on an image in the image library"
            </span>
          </h2>
        ) : ( //else return image alt text
          <div>
            <h2>
              Current Image: <em>{alt}</em>
            </h2>
            <p>
              Click on text to drag
            </p>
          </div>
        )}

        <div className="img-canvas">
          <img id="imageCanvas" src={src} width={7 * 50} height={7 * 50} />
          <div id="toptext">{this.props.topText}</div>
          <div id="bottomtext">{this.props.bottomText}</div>
          <div id="toptexttwo">{this.props.topTextTwo}</div>
          <div id="bottomtexttwo">{this.props.bottomTextTwo}</div>
        </div>
      </div>
    );
  }
}
export default CurrentImage;

//Props being passed in from page, where this Component is being used.
CurrentImage.defaultProps = {
  currentImage: undefined,
  topText: undefined,
  bottomText: undefined,
  topTextTwo: undefined,
  bottomTextTwo: undefined
};

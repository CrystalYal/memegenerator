import React, { Component } from "react";
import { withRouter } from 'react-router-dom';
import TextForm from "./TextForm";
import CurrentImage from "./CurrentImage";
import ImageLibrary from "./ImageLibrary";
import Header from "./Header";
import "./App.css";

class MemeGenerator extends Component {
  state = {
    allImages: [],
    currentImage: "",
    allEdits: [],
    topText: "",
    bottomText: "",
    topTextTwo: "",
    bottomTextTwo: ""
  };

  generateMeme = () =>{
    const currentImage = this.state.currentImage;
    const formData = new FormData();
    let top = this.state.topText;
    let bottom = this.state.bottomText;
//The FormData interface provides a way to easily construct a set of key/value pairs
// representing form fields and their values
    formData.append('username', 'crystalz411');
    formData.append('password', 'validus1234');
    formData.append('template_id', currentImage.id);
    formData.append('text0',top );
    formData.append('text1', bottom);
        
    fetch('https://api.imgflip.com/caption_image', {
        method: 'POST',
        body: formData
      }).then(res => {
        res.json().then(res => {
          let url = res.data.url;
          window.open(url, '_blank');
          console.log(res);
        });
      });
    };

  componentDidMount() {
      //used Imgflip API https://imgflip.com/api
    fetch("https://api.imgflip.com/get_memes")
      .then((res) => res.json())
      .then((output) => {
        let imgArray = [];
        let memes = output.data.memes;

        for (let i = 0; i < memes.length; i++) {
          if (i < 30) {
            imgArray.push(memes[i]);
          }
        }
        this.setState({
          allImages: imgArray,
        });
      })
      .catch((err) => console.log(err));
  }

//NOT NEEDED, ONLY USED BECAUSE I PREVIOUSLY TRIEND TO UPLOAD IMAGE WITH CANVAS
//The HTML <canvas> element is used to draw graphics, on the fly, via JavaScript.  

// loadImage = (event) => {
  //   const img = document.getElementById("imageCanvas");    
  //   img.src = event ? event.target.src : this.state.currentImage.src;
  //    return (this.state.currentImage)
  // };



  handleImgClick = (e) => {
    console.log("img target",e)
    //target is the image id
    this.setState({ currentImage: e.target });

    // this.loadImage(e);
  };

  handleChangeTopText = e => {
      this.setState({
          topText: e.target.value
      });
  };

  handleChangeBottomText = e => {
    this.setState({
        bottomText: e.target.value
    });
};

handleChangeTopTextTwo = e => {
    this.setState({
        topTextTwo: e.target.value
    });
};

handleChangeBottomTextTwo = e => {
  this.setState({
      bottomTextTwo: e.target.value
  });
};


  render() {
    return (
      <div className="meme-generator">
        <Header/>
        <div className="memeBox">
          <div className="top-container">
            <div className="img-container">
              <CurrentImage              
                currentImage={this.state.currentImage}
                topText = {this.state.topText}
                bottomText= {this.state.bottomText}
                topTextTwo = {this.state.topTextTwo}
                bottomTextTwo= {this.state.bottomTextTwo}
              />
            </div>

            <div className="right-container">
              <ImageLibrary
                allImages={this.state.allImages}
                handleImgClick={this.handleImgClick}
              />
              <div className="text-container">
              <TextForm
                userText={this.state.topText}
                handleTextChange={this.handleChangeTopText}
              />
              <TextForm
                userText={this.state.bottomText}
                handleTextChange={this.handleChangeBottomText}
              />
              <TextForm
                userText={this.state.topTextTwo}
                handleTextChange={this.handleChangeTopTextTwo}
              />
              <TextForm
                userText={this.state.bottomTextTwo}
                handleTextChange={this.handleChangeBottomTextTwo}
              />
              </div>
              <br/>
              <button onClick={this.generateMeme}>Generate!</button>
            </div>
            
          </div>
          
        </div>
      </div>
    );
  }
}

export default MemeGenerator;
